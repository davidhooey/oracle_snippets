# Oracle Snippets for Visual Studio Code

A set of snippets to generate Oracle scripts.

## Snippets

All snippets are global and are triggered with `oracle`.

* oracle_waits_for_sqlid
* oracle_execution_plan_for_sqlid
* oracle_blocking_sessions
* oracle_segment_advisor_for_schema
* oracle_segment_shrink

## Setup

1. Clone the respository

2. Within the cloned directory, create a hard link or copy the 
   `oracle.code-snippets` file to the Visual Studio snippet location. Soft links
   do not work for some reason.

      UNIX/Mac Link:
      ```bash
      ln ./oracle.code-snippets ~/Library/Application\ Support/Code/User/snippets/oracle.code-snippets
      ```
   
      UNIX/Mac Copy:
      ```bash
      cp ./oracle.code-snippets ~/Library/Application\ Support/Code/User/snippets/oracle.code-snippets
      ```

      Windows Copy:
      ```
      copy oracle.code-snippets %HOMEPATH%\AppData\Roaming\Code\User\snippets
      ```